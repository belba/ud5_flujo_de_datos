package ud5_Flujo_de_datos;

import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		/* 3) Modifica la aplicación anterior, para que nos pida el nombre que queremos introducir
		(recuerda usar JOptionPane). */
		
	    Scanner nombre = new Scanner(System.in); 
	    System.out.print("Introduce nombre: ");
				
		System.out.println("Bienvenido "+ nombre.nextLine());
		
		nombre.close();
	}

}
