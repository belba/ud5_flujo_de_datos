package ud5_Flujo_de_datos;

import java.util.Scanner;

public class Ex5 {

	public static void main(String[] args) {		
		/*5) Lee un n�mero por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es, tambi�n
			debemos indicarlo. */
		
		Scanner numero = new Scanner(System.in);
		System.out.print("Introduce n�mero: ");
		
		int numeroIntroducido = numero.nextInt(); 
		int resto  = numeroIntroducido%2;
		
		if(resto != 0) {
			System.out.println(numeroIntroducido + " No es divisible entre 2");
		}else {
			System.out.println(numeroIntroducido + " Es divisible entre 2");
		}		
		
		numero.close();
		
	}

}
