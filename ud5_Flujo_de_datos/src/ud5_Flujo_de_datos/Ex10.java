package ud5_Flujo_de_datos;

import java.util.Scanner;

public class Ex10 {

	public static void main(String[] args) {
		/*
		 * 10) Realiza una aplicaci�n que nos pida un n�mero de ventas a introducir, despu�s nos
		pedir� tantas ventas por teclado como n�mero de ventas se hayan indicado. Al final
	    mostrara la suma de todas las ventas. Piensa que es lo que se repite y lo que no.
		*/

		Scanner ventas = new Scanner(System.in); 
		System.out.print("Introduce el n�mero de ventas: ");
		
		int numeroVentas = ventas.nextInt();
		int sumarVentas = 0;
		
		for (int j = 1; j <= numeroVentas; j++) {
			Scanner cantidadVenta = new Scanner(System.in); 
			System.out.print("Venta "+j+" introduce la cantidad: ");
			
			sumarVentas = cantidadVenta.nextInt() + sumarVentas;			
		}
		
		System.out.print("Suma total de ventas: "+sumarVentas);
		ventas.close();
	}

}
