package ud5_Flujo_de_datos;

import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		/*4) Haz una aplicaci�n que calcule el �rea de un circulo (pi*R2). El radio se pedir� por teclado
			(recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el
			m�todo pow de Math. */		
		
		Scanner radio = new Scanner(System.in); 
	    System.out.print("Introduce el radio: ");
				
	    double radioDouble = radio.nextDouble();
	    	    
	    double area =  Math.PI*Math.pow(radioDouble, 2);
	    
		System.out.println("Resultado: "+ area);
		
		radio.close();
		    
	}
}
