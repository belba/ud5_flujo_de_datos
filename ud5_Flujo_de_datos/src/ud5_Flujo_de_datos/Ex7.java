package ud5_Flujo_de_datos;

public class Ex7 {

	public static void main(String[] args) {
		/*
		 * 7) Muestra los n�meros del 1 al 100 (ambos incluidos). Usa un bucle while.
		 */
		
		int i = 1;
		
		while(i<=100) {
			System.out.println("Numero: "+i);
			i++;
		}
	}

}
