package ud5_Flujo_de_datos;

import java.util.Scanner;

public class Ex12 {

	public static void main(String[] args) {		
		/*12) Escribe una aplicaci�n con un String que contenga una contrase�a cualquiera. Despu�s
		se te pedir� que introduzcas la contrase�a, con 3 intentos. Cuando aciertes ya no pedir� mas
		la contrase�a y mostrara un mensaje diciendo �Enhorabuena�. Piensa bien en la condici�n
		de salida (3 intentos y si acierta sale, aunque le queden intentos). */
		
		Scanner contra = new Scanner(System.in); 
		System.out.print("Introduce una contrase�a cualquiera: ");
		
		String contras = contra.nextLine();
		
		int intentos = 3;
		
		while(intentos > 0) {
			
			Scanner clave = new Scanner(System.in); 
			System.out.print("Introduce una contrase�a (Tiene "+intentos+" intentos): ");
			
			String claveCorrecta = clave.nextLine();
			
			if(claveCorrecta.equals(contras)) {
				System.out.print("Enhorabuena");
				clave.close();
				intentos = 0;
			}else {
				System.out.println("Ha introducido la contrase�a incorrecta");
				intentos--;				
			}
		}
		contra.close();
		
		
	}

}
