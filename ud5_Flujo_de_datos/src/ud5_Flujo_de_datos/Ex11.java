package ud5_Flujo_de_datos;

import java.util.Scanner;

public class Ex11 {

	public static void main(String[] args) {
		/*11) Crea una aplicaci�n que nos pida un d�a de la semana y que nos diga si es un d�a laboral
			o no. Usa un switch para ello. */
		
		Scanner dia = new Scanner(System.in); 
		System.out.print("Introduce un d�a de la semana (La primera letra en may�scula): ");
		
		String diaSemana = dia.nextLine();
		
		 switch (diaSemana) 
	        {
	            case "Lunes": 
	            	System.out.println("D�a laborable");
	                break;
	            case "Martes": 
	            	System.out.println("D�a laborable");
	                break;
	            case "Miercoles": 
	            	System.out.println("D�a laborable");
	                break;
	            case "Jueves": 
	            	System.out.println("D�a laborable");
	                break;
	            case "Viernes": 
	            	System.out.println("D�a laborable");
	                break;
	            case "Sabado": 
	            	System.out.println("D�a no laborable");
	                break;	                
	            case "Domingo": 
	            	System.out.println("D�a no laborable");
	                break;
	            default: 
	            	System.out.println("Dia invalido");
	                break;
	        }
		
		 dia.close();
	}

}
