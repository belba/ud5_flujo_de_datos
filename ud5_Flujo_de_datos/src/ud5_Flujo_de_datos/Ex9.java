package ud5_Flujo_de_datos;

public class Ex9 {

	public static void main(String[] args) {
		/*
		 * 9) Muestra los n�meros del 1 al 100 (ambos incluidos) divisibles entre 2 y 3. Utiliza el bucle que desees.
		 */
				
		for(int i=1; i<=100; i++) {
			
			if(i%2 == 0) {
				System.out.println(i + " Es divisible entre 2");
			}
			
			if(i%3 == 0) {
				System.out.println(i + " Es divisible entre 3");
			}		
		}
	}

}
