package ud5_Flujo_de_datos;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Ex13 {

	public static void main(String[] args) {
		/*
		 * 13) Crea una aplicaci�n llamada CalculadoraInversa, nos pedir� 2 operandos (int) y un signo
			aritm�tico (String), seg�n este �ltimo se realizara la operaci�n correspondiente. Al final
			mostrara el resultado en un cuadro de dialogo.
			Los signos aritm�ticos disponibles son:
			+: suma los dos operandos.
			-: resta los operandos.
			*: multiplica los operandos.
			/: divide los operandos, este debe dar un resultado con decimales (double)
			^: 1� operando como base y 2� como exponente.
			%: m�dulo, resto de la divisi�n entre operando1 y operando2
		 */
		
		Scanner operador1 = new Scanner(System.in); 
		System.out.print("Introduce primer operador: ");
		int oper1 = operador1.nextInt();
		
		Scanner operador2 = new Scanner(System.in); 
		System.out.print("Introduce segundo operador: ");
		int oper2 = operador2.nextInt();
		
		Scanner signo = new Scanner(System.in); 
		System.out.print("Introduce signo aritmetico: ");		
		String signoArt = signo.nextLine();
		
		switch (signoArt) {
		case "+":
			int resultadoSuma = oper1 + oper2;
			System.out.println("Resultado: "+resultadoSuma);
			break;
		case "-":
			int resultadoResta = oper1 - oper2;
			System.out.println("Resultado: "+resultadoResta);
			break;
		case "*":
			int resultadoMultiplica = oper1 * oper2;
			System.out.println("Resultado: "+resultadoMultiplica);
			break;
		case "/":
			float resultadoDivide = oper1 / (float) oper2;
			System.out.println("Resultado: "+resultadoDivide);
			break;
		case "^":
			double resultadoOperador = Math.pow(oper1, oper2);
			System.out.println("Resultado: "+resultadoOperador);
			break;
		case "%":
			Double resultadoModulo = (double) ((oper1 * oper2)/100);
			System.out.println("Resultado: "+ resultadoModulo);
			break;
			
		default:
			System.out.println("Signo incorrecto");
			break;
		}
		operador1.close();
		operador2.close();
		signo.close();
	}

}
