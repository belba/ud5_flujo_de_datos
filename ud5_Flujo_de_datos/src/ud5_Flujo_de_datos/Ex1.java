package ud5_Flujo_de_datos;

public class Ex1 {

	public static void main(String[] args) {
		/*1) Declara 2 variables num�ricas (con el valor que desees), he indica cual es mayor de los
		dos. Si son iguales indicarlo tambi�n. Ves cambiando los valores para comprobar que
		funciona.*/

		int v1 = 20;
		int v2 = 40;
		
		if(v1>v2) {
			System.out.println("El v1: "+ v1 +" es mayor que v2: "+v2);
		}
		if(v1<v2) {
			System.out.println("El v2: "+ v2 +" es mayor que v1: "+v1);
		}
		if(v1==v2) {
			System.out.println("El v1: "+ v1 +" es igual que v2: "+v2);
		}
	}

}
